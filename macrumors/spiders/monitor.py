# -*- coding: utf-8 -*-
import io
import csv
import scrapy
import requests
import ahocorasick
from scrapy.spiders import XMLFeedSpider
from scrapy.exceptions import CloseSpider


class MonitorSpider(XMLFeedSpider):
    name = 'monitor'
    allowed_domains = ['forums.macrumors.com']
    start_urls = ['https://forums.macrumors.com/forums/-/index.rss']
    iterator = 'iternodes'
    itertag = 'item'


    def start_requests(self):
        if 'KEYWORDS_FILE_URI' not in self.settings:
            raise CloseSpider("You must specificy KEYWORDS_FILE_URI file in settings.py")

        uri = self.settings['KEYWORDS_FILE_URI']
        user_agent = self.settings.get('KEYWORDS_FILE_URI', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36')
        headers = {
            'User-Agent': user_agent,
        }

        r = requests.get(uri, headers=headers)

        if r.status_code != 200:
            raise CloseSpider("Unable to get keywords file. Unsupported HTTP code: {}".format(r.status_code))

        dr = csv.DictReader(io.StringIO(r.text))
        self.auto = ahocorasick.Automaton()

        for line in dr:
            key = line['PartNumber'].strip().upper()
            self.auto.add_word(key, key)

        # tesing
        # self.auto.add_word('EMOJI', 'EMOJI')

        self.auto.make_automaton()
        self.logger.info('Keywords have been added: {}'.format(self.auto.get_stats()['words_count']))

        for url in self.start_urls:
            yield scrapy.Request(url)


    def parse_node(self, response, node):
        url = node.xpath('link/text()').get()
        title = node.xpath('title/text()').get()

        text = ' '.join(filter(None, [t.strip() for t in node.xpath('//*/text()').getall()]))
        upper_text = text.upper()

        matches = list(set([t[1] for t in self.auto.iter(upper_text)]))

        if matches:
            return {
                'url': url,
                'title': title,
                'text': text,
                'matches': matches,
            }
