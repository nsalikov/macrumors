import datetime
from scrapy import signals
from scrapy.mail import MailSender
from scrapy.exceptions import NotConfigured


class CustomStatsMailer(object):

    items = []

    email_template = """
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html lang="en">

        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <title>
            {header}
          </title>

          <style type="text/css">
          </style>
        </head>

        <body style="margin:0; padding:0;">
            <table width="100%" border="1" cellpadding="0" cellspacing="0" bgcolor="#F2F2F2" width="600" style="max-width:600px; width:100%;">
                <th colspan="2" style="text-align: center;">
                    {header}
                </th>
                {trs}
            </table>
        </body>
        </html>

    """

    tr_template = """
        <tr style="text-align: left;">
          <td class="cell">{key}</td>
          <td class="cell">{value}</td>
        </tr>
    """

    def __init__(self, stats, recipients, mail):
        self.stats = stats
        self.recipients = recipients
        self.mail = mail


    @classmethod
    def from_crawler(cls, crawler):
        recipients = crawler.settings.getlist("STATSMAILER_RCPTS", [])

        if not recipients:
            raise NotConfigured

        mail = MailSender.from_settings(crawler.settings)
        o = cls(crawler.stats, recipients, mail)

        crawler.signals.connect(o.item_scraped, signal=signals.item_scraped)
        crawler.signals.connect(o.spider_closed, signal=signals.spider_closed)

        return o


    def item_scraped(self, item, spider):
        self.items.append(item)


    def spider_closed(self, spider):
        if not self.items:
            return

        subject = '[macrumors] {} posts have been found'.format(len(self.items))

        dt = datetime.datetime.now().strftime("%Y/%m/%d %H:%M")
        header = "Summary stats on {}".format(dt)

        trs = []
        for item in self.items:
            key = '<a href="{url}">{title}</a>'.format(url=item['url'], title=item['title'])
            value = ', '.join(item['matches'])
            trs.append(self.tr_template.format(key=key, value=value))

        trs = '\n'.join(trs)
        body = self.email_template.format(header=header, trs=trs)

        self.recipients = list(set(self.recipients))

        return self.mail.send(self.recipients, subject=subject, body=body, mimetype='text/html')
